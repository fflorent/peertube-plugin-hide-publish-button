function register ({ registerHook, peertubeHelpers }) {
  const togglePublishButton = getTogglePublishButton()

  const checkServerConfig = () =>
    peertubeHelpers.getServerConfig()
      .then(({ user }) =>
        togglePublishButton({ show: user.videoQuota !== 0 }))

  checkServerConfig()

  registerHook({
    target: 'action:auth-user.information-loaded',
    handler: ({ user }) =>
      togglePublishButton({ show: user.videoQuota !== 0 })
  })

  registerHook({
    target: 'action:auth-user.logged-out',
    handler: checkServerConfig
  })
}

const getTogglePublishButton = () => {
  let waiter

  return ({ show }) => {
    if (waiter) clearTimeout(waiter)

    waiter = setTimeout(() => {
      if (show) {
        document.body.classList.add('show-publish-button')
      } else {
        document.body.classList.remove('show-publish-button')
      }
    }, 500)
  }
}

export {
  register
}
